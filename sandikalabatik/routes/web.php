<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UpdateController;
use App\Http\Controllers\contactController;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::post('/kontak', [contactController::class , 'save']);

Route::post('/midtrans-callback', [UpdateController::class , 'callback']);

Route::get('/', App\Http\Livewire\Home::class);
Route::get('/TambahProduk', App\Http\Livewire\TambahProduk::class);
Route::get('/about', App\Http\Livewire\About::class);
Route::get('/about/edit/{id}', App\Http\Livewire\About::class);
Route::get('/kontak', App\Http\Livewire\contact::class);
Route::get('/shop', App\Http\Livewire\Shop::class);
Route::get('/BelanjaUser', App\Http\Livewire\BelanjaUser::class);
Route::get('/Bayar/{id}', App\Http\Livewire\Bayar::class);
Route::get('/TambahOngkir/{id}', App\Http\Livewire\TambahOngkir::class);
Route::get('/coba', App\Http\Livewire\coba::class);
Route::get('/edit', App\Http\Livewire\Edit::class);
Route::get('/hapus', App\Http\Livewire\hapus::class);
Route::get('/detail/{id}', App\Http\Livewire\Detailpertanyaan::class);
//Route::get('/UpdateData/{id}', App\Http\Livewire\UpdateData::class);
Route::get('/UpdateData/{id}', [UpdateController::class, 'data']);
Route::post('/update/{id}',[UpdateController::class, 'update'] )->name('update');
Route::post('/save/{id}', [UpdateController::class , 'save']);
Route::get('/sukses', [UpdateController::class, 'sukses']);
Route::get('/logout', [UpdateController::class, 'logout']);
Route::get('/contact', [UpdateController::class, 'contact']);
Route::post('/save', [UpdateController::class, 'savecontact']);
Route::get('/pertanyaan', [UpdateController::class, 'pertanyaan']);
Route::get('/detilpertanyaan/{id}', [UpdateController::class, 'detilpertanyaan']);
Route::get('/destroy/{id}', [UpdateController::class, 'destroy']);
Route::get('/hapus', [UpdateController::class, 'hapus']);
Route::get('/TambahkanOngkir/{id}',App\Http\Livewire\TambahkanOngkir::class);



