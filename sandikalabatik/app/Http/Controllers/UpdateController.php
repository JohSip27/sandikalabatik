<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Models\belanja;
use App\Models\contact;
use App\Models\kontak;
use Livewire\Commands\ComponentParser;

class UpdateController extends Controller
{
    public function callback(Request $request)
    {
        $serverKey = 'SB-Mid-server-EUoAWXWpqbh8F6FslOC_IRbu';
        $hashed = hash("sha512", $request->order_id.$request->status_code.$request->gross_amount.$serverKey);
        if($hashed == $request->signature_key) {
            $order = belanja::find($request->id);
            $order->update(['status' => '2']);
        }
    }

    public function data($id)
    {
        $data = belanja::find($id);
        return view('update', compact('data'));
    }

    public function detilpertanyaan($id)
    {
        $data = kontak::find($id);
        return view('detil' ,compact('data'));
    }

    public function save(Request $request, $id)
    {
        $data = belanja::find($id);
        $data->update($request->all());
        return view('sukses');
        
    }

    public function savecontact(Request $request)
    {
        $request->validate([
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'subject' => 'required',
            'message' => 'required',
        ]);

        $data = contact::create($request->all());
        return redirect('/contact')->with('success','Feedback Berhasil Dikirim');
    }

    // public function hapus()
    // {
    //     return view ('hapus');
    // }


    public function sukses()
    {
        return view('sukses');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function contact()
    {
        return view('contact');
    }

    public function pertanyaan()
    {
        return view('pertanyaan');
    }

    public function destroy($id)
    {
        $data = kontak::find($id);
        $data->delete();
        return view('livewire.hapus')->with('success','Data Berhasil Di Hapus');
    }
}
