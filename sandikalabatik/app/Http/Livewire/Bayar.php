<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Illuminate\Support\Facades\Auth;
use App\Models\belanja;


class Bayar extends Component
{
    public $snapToken;
    public $belanja;

    public $va_number,$gross_amount,$transaction_status,$deadline,$bank;
    public function mount($id) 
    {
        belanja::find($id);
        if(!Auth::user())
        {
            return redirect()->route('login');
        }

                // Set your Merchant Server Key
\Midtrans\Config::$serverKey = 'SB-Mid-server-EUoAWXWpqbh8F6FslOC_IRbu';
// Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
\Midtrans\Config::$isProduction = false;
// Set sanitization on (default)
\Midtrans\Config::$isSanitized = true;
// Set 3DS transaction for credit card to true
\Midtrans\Config::$is3ds = true;


        if(isset($_GET['result_data']))
        {
            $current_status = json_decode($_GET['result_data'],true);
            $order_id = $current_status['order_id'];
            $this->belanja = belanja::where('id', $order_id)->first();
            $this->belanja->status = 2;
            $this->belanja->update();
        }
        else {            
            //ambil data belanja
            $this->belanja = belanja::find($id);
        }

        if(!empty($this->belanja))
        {

            if($this->belanja->status == 1)
            {
                $params = array(
                    'transaction_details' => array(
                        'order_id' => $this->id,
                        'gross_amount' => $this->belanja->total_harga,
                    ),
                    'customer_details' => array(
                        'first_name' => 'Saudara ',
                        'last_name' => Auth::user()->name,
                        'email' =>  Auth::user()->email,
                    ),
                );
    
                $this->snapToken = \Midtrans\Snap::getSnapToken($params);

            }
            else if($this->belanja->status == 2)
            {
                $status = \Midtrans\Transaction::status($this->belanja->id);
                $status = json_decode(json_encode($status),true);
                

                $this->va_number = $status['va_number'][0]['va_number'];
                $this->gross_amount = $status['gross_amount'];
                $this->bank = $status['va_numbers'][0]['bank'];
                $this->transaction_status = $status['transaction_status'];
                $transaction_time = $status['transaction_time'];
                $this->deadline = date('Y-m-d H:i:s', strtotime('+1 day',strtotime($transaction_time)));
            }
        }

    }

    public function render()
    {

       
        return view('livewire.bayar'
        )
        ->extends('layouts.app')->section('content');

    }
}
