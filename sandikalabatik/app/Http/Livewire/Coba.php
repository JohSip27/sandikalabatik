<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Coba extends Component
{
    public $firstname;
    public $lastname;
    public $email;
    public $subject;
    public $message;
    public function render()
    {
        return view('livewire.coba')
        ->extends('layouts.app')->section('content');

    }
}
