<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;
use App\Models\belanja;

use Livewire\Component;

class About extends Component
{


    public function render()
    {

        return view('livewire.about', [
            'belanja' => belanja::all()
        ])
        ->extends('layouts.app')->section('content');

    }
}
