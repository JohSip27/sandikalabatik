<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;

use Livewire\Component;
use App\Models\belanja;
use Kavist\RajaOngkir\RajaOngkir;

class TambahkanOngkir extends Component
{

    public $belanja;
    public $provinsi,$kota_id,$jasa, $daftarProvinsi, $daftarKota, $nama_jasa;

    private $apiKey = '0a15a0792ae5a57c3ba076949db6bb8b';

    public function mount($id)
    {
        if(!Auth::user())
        {
            return redirect()->route('/'); 
        }
        $this->belanja = Belanja::find($id);
    }
    public function render()
    {
        $rajaOngkir = new RajaOngkir($this->apiKey);
        $this->daftarProvinsi = $rajaOngkir->provinsi()->all();
        


        
        
$this->daftarKota = $rajaOngkir->kota()->dariProvinsi($this->provinsi)->get();
        

        return view('livewire.tambahkan-ongkir')
        ->extends('layouts.app')->section('content');

    }
}
