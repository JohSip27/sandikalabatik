<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\kontak;

class Detailpertanyaan extends Component
{

    public function detail($id)
    {
        $data = kontak::find($id);
        return view ('livewire.detailpertanyaan', compact('data'));
    }
    public function render()
    {
        
        return view('livewire.detailpertanyaan')
        ->extends('layouts.app')->section('content');

    }
}
