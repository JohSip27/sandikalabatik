<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UpdateData extends Component
{


    public function render()
    {
        return view('livewire.update-data')
        ->extends('layouts.app')->section('content');

    }
}
