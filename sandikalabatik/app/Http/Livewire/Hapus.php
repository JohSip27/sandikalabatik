<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Hapus extends Component
{
    public function render()
    {
        return view('livewire.hapus')
        ->extends('layouts.app')->section('content');

    }
}
