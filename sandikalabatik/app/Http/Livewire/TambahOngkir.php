<?php

namespace App\Http\Livewire;
use App\Models\belanja;
use App\Models\produk;
use Livewire\Component;
use Kavist\RajaOngkir\RajaOngkir;
use Illuminate\Support\Facades\Auth;


class TambahOngkir extends Component
{
    public $belanja;
    private $apikey = '0a15a0792ae5a57c3ba076949db6bb8b';
    public $provinsi_id,$kota_id,$jasa, $daftarProvinsi, $daftarKota, $nama_jasa;
    public $result = [];
    public function mount($id) 
    {
        if(!Auth::user())
        {
            return redirect()->route('login');
        }
        $this->belanja = belanja::find($id);
        if (!$this->belanja) {
            return;
        }

        if($this->belanja->user_id != Auth::user()->id)
        {
            return redirect()->to('');
        }
        
        
    }

    public function getOngkir()
    {
        if(!$this->provinsi_id || !$this->kota_id || !$this->jasa)
        {
            return;
        }

        $produk = produk::find($this->belanja->produk_id);
        $rajaOngkir = new RajaOngkir($this->apikey);
        $cost = $rajaOngkir->ongkosKirim([
            'origin' => 489,
            'destination' => $this->kota_id,
            'weight' => $produk->berat,
            'courier' => $this->jasa
        ])->get();


         

        $this->nama_jasa = $cost[0]['name'];

        //dd($this->nama_jasa);

        foreach($cost[0]['costs'] as $row)
        {
            $this->result[] = array(
                'description' => $row['description'],
                'biaya' => $row['cost'][0]['value'],
                'etd' => $row['cost'][0]['etd']
            );
        }

        //dd($this->result);

        
    }

    public function save_ongkir($biaya_pengiriman)
    {
        $this->belanja->total_harga += $biaya_pengiriman;
        $this->belanja->status = 1;
        $this->belanja->update();

        return redirect()->to('BelanjaUser');
    }
    public function render()
    {
        $rajaOngkir = new RajaOngkir($this->apikey);
        $this->daftarProvinsi = $rajaOngkir->provinsi()->all();

       // $this->daftarProvinsi = 1;

        if($this->provinsi_id)
        {
            $this->daftarKota = $rajaOngkir->kota()->dariProvinsi($this->provinsi_id)->get();
        }
        return view('livewire.tambah-ongkir')
        ->extends('layouts.app')->section('content');

    }
}
