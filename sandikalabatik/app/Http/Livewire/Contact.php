<?php

namespace App\Http\Livewire;
use Illuminate\Support\Facades\Auth;

use App\Models\kontak;
use Livewire\Component;


class Contact extends Component
{



    public function render()
    {
        $data = kontak::all();


        return view('livewire.kontak', compact('data'))
        ->extends('layouts.app')->section('content');

    }
}
