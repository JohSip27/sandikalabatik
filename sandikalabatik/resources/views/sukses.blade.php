@if(Auth::user())
        @if(Auth::user()->level == 1)
        <!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Admin Sandikala</title>
  <!-- base:css -->
  <link rel="stylesheet" href="{{ asset('admin') }}/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="{{ asset('admin') }}/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="{{ asset('admin') }}/css/style.css">
  <!-- endinject -->
  <link rel="icon" href="{{asset('gambar')}}/sandikala.png" />
</head>
<body>
  <div class="container-scroller d-flex">
    <!-- partial:./partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item sidebar-category">
          <p>Navigation</p>
          <span></span>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/">
            <i class="mdi mdi-view-quilt menu-icon"></i>
            <span class="menu-title">Dashboard</span>
            <div class="badge badge-info badge-pill">2</div>
          </a>
        </li>
        <li class="nav-item sidebar-category">
          <p>Components</p>
          <span></span>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="/TambahProduk">
            <i class="mdi mdi-view-headline menu-icon"></i>
            <span class="menu-title">Tambah Produk</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="pages/charts/chartjs.html">
            <i class="mdi mdi-chart-pie menu-icon"></i>
            <span class="menu-title">Pertanyaan Customer</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about">
            <i class="mdi mdi-grid-large menu-icon"></i>
            <span class="menu-title">Produk</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/shop">
            <i class="mdi mdi-emoticon menu-icon"></i>
            <span class="menu-title">Data Customer</span>
          </a>
        </li>
        
        
      </ul>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:./partials/_navbar.html -->
      <nav class="navbar col-lg-12 col-12 px-0 py-0 py-lg-4 d-flex flex-row">
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <div class="navbar-brand-wrapper">

            <a class="navbar-brand brand-logo-mini" href="index.html"><img src="images/logo-mini.svg" alt="logo"/></a>
          </div>
          <h4 class="font-weight-bold mb-0 d-none d-md-block mt-1 m-5"> Welcome back,  {{ Auth::user()->name }}</h4>
          <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item">
              <h4 class="mb-0 font-weight-bold d-none d-xl-block">Dashboard Sandikala Batik</h4>
            </li>
            <li class="nav-item dropdown mr-1">
              <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" data-toggle="dropdown">
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                <a class="dropdown-item preview-item">
                  
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal">David Grey
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      The meeting is cancelled
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                      <img src="images/faces/face2.jpg" alt="image" class="profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal">Tim Cook
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      New product launch
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                      <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal"> Johnson
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      Upcoming board meeting
                    </p>
                  </div>
                </a>
              </div>
            </li>
            <li class="nav-item dropdown mr-2">
              <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center" id="notificationDropdown" href="#" data-toggle="dropdown">
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-success">
                      <i class="mdi mdi-information mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">Application Error</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      Just now
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-warning">
                      <i class="mdi mdi-settings mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">Settings</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      Private message
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-info">
                      <i class="mdi mdi-account-box mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">New user registration</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      2 days ago
                    </p>
                  </div>
                </a>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
        <div class="navbar-menu-wrapper">
          
              
            </li>
            
          </ul>
        </div>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">

            <div class="container mt-5">
              <div class="card">
                <div class="card-header">
                  <h3 class="text-center">Sukses</h3>
                </div>
                <div class="card-body">
                <div class="alert alert-primary" role="alert">
                    Sukses, Data Pemesanan Berhasil Dikonfirmasi
                </div>
                <a href="/about" class="btn btn-success">Kembali</a>
                </div>
              </div>
            </div>
            </div>
          </div>
          
          <!-- row end -->
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:./partials/_footer.html -->
        <footer class="footer">
          <div class="card">
            <div class="card-body">
                <span class="text-dark d-block text-center text-sm-center d-sm-inline-block">Copyright © SandikalaBatik.com 2023</span>
            </div>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- base:js -->
  <script src="{{ asset('admin') }}/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="{{ asset('admin') }}/vendors/chart.js/Chart.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{ asset('admin') }}/js/off-canvas.js"></script>
  <script src="{{ asset('admin') }}/js/hoverable-collapse.js"></script>
  <script src="{{ asset('admin') }}/js/template.js"></script>
  <!-- endinject -->
  <!-- plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- Custom js for this page-->
  <script src="{{ asset('admin') }}/js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>
        @endif
@endif

@if(Auth::user())
        @if(Auth::user()->level == 0)
        <!DOCTYPE html>
        <html lang="en">
        
        <head>
          <!-- Required meta tags -->
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <title>Admin Sandikala</title>
          <!-- base:css -->
          <link rel="stylesheet" href="{{ asset('admin') }}/vendors/mdi/css/materialdesignicons.min.css">
          <link rel="stylesheet" href="{{ asset('admin') }}/vendors/css/vendor.bundle.base.css">
          <!-- endinject -->
          <!-- plugin css for this page -->
          <!-- End plugin css for this page -->
          <!-- inject:css -->
          <link rel="stylesheet" href="{{ asset('admin') }}/css/style.css">
          <!-- endinject -->
          <link rel="shortcut icon" href="{{ asset('admin') }}/images/favicon.png" />
        </head>
        <body>
          <div class="container-scroller d-flex">
            <!-- partial:./partials/_sidebar.html -->
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
              <ul class="nav">
                <li class="nav-item sidebar-category">
                  <p>Navigation</p>
                  <span></span>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/">
                    <i class="mdi mdi-view-quilt menu-icon"></i>
                    <span class="menu-title">Dashboard</span>
                    <div class="badge badge-info badge-pill">2</div>
                  </a>
                </li>
                <li class="nav-item sidebar-category">
                  <p>Components</p>
                  <span></span>
                </li>
                
                <li class="nav-item">
                  <a class="nav-link" href="/TambahProduk">
                    <i class="mdi mdi-view-headline menu-icon"></i>
                    <span class="menu-title">Tambah Produk</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="pages/charts/chartjs.html">
                    <i class="mdi mdi-chart-pie menu-icon"></i>
                    <span class="menu-title">Pertanyaan Customer</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/about">
                    <i class="mdi mdi-grid-large menu-icon"></i>
                    <span class="menu-title">Produk</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="/shop">
                    <i class="mdi mdi-emoticon menu-icon"></i>
                    <span class="menu-title">Data Customer</span>
                  </a>
                </li>
                
                
              </ul>
            </nav>
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
              <!-- partial:./partials/_navbar.html -->
              <nav class="navbar col-lg-12 col-12 px-0 py-0 py-lg-4 d-flex flex-row">
                <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                  <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
                    <span class="mdi mdi-menu"></span>
                  </button>
                  <div class="navbar-brand-wrapper">
        
                    <a class="navbar-brand brand-logo-mini" href="index.html"><img src="images/logo-mini.svg" alt="logo"/></a>
                  </div>
                  <h4 class="font-weight-bold mb-0 d-none d-md-block mt-1 m-5"> Welcome back,  {{ Auth::user()->name }}</h4>
                  <ul class="navbar-nav navbar-nav-right">
                    <li class="nav-item">
                      <h4 class="mb-0 font-weight-bold d-none d-xl-block">Dashboard Sandikala Batik</h4>
                    </li>
                    <li class="nav-item dropdown mr-1">
                      <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" data-toggle="dropdown">
                      </a>
                      <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                        <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                        <a class="dropdown-item preview-item">
                          
                          <div class="preview-item-content flex-grow">
                            <h6 class="preview-subject ellipsis font-weight-normal">David Grey
                            </h6>
                            <p class="font-weight-light small-text text-muted mb-0">
                              The meeting is cancelled
                            </p>
                          </div>
                        </a>
                        <a class="dropdown-item preview-item">
                          <div class="preview-thumbnail">
                              <img src="images/faces/face2.jpg" alt="image" class="profile-pic">
                          </div>
                          <div class="preview-item-content flex-grow">
                            <h6 class="preview-subject ellipsis font-weight-normal">Tim Cook
                            </h6>
                            <p class="font-weight-light small-text text-muted mb-0">
                              New product launch
                            </p>
                          </div>
                        </a>
                        <a class="dropdown-item preview-item">
                          <div class="preview-thumbnail">
                              <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                          </div>
                          <div class="preview-item-content flex-grow">
                            <h6 class="preview-subject ellipsis font-weight-normal"> Johnson
                            </h6>
                            <p class="font-weight-light small-text text-muted mb-0">
                              Upcoming board meeting
                            </p>
                          </div>
                        </a>
                      </div>
                    </li>
                    <li class="nav-item dropdown mr-2">
                      <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center" id="notificationDropdown" href="#" data-toggle="dropdown">
                      </a>
                      <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                        <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                        <a class="dropdown-item preview-item">
                          <div class="preview-thumbnail">
                            <div class="preview-icon bg-success">
                              <i class="mdi mdi-information mx-0"></i>
                            </div>
                          </div>
                          <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-normal">Application Error</h6>
                            <p class="font-weight-light small-text mb-0 text-muted">
                              Just now
                            </p>
                          </div>
                        </a>
                        <a class="dropdown-item preview-item">
                          <div class="preview-thumbnail">
                            <div class="preview-icon bg-warning">
                              <i class="mdi mdi-settings mx-0"></i>
                            </div>
                          </div>
                          <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-normal">Settings</h6>
                            <p class="font-weight-light small-text mb-0 text-muted">
                              Private message
                            </p>
                          </div>
                        </a>
                        <a class="dropdown-item preview-item">
                          <div class="preview-thumbnail">
                            <div class="preview-icon bg-info">
                              <i class="mdi mdi-account-box mx-0"></i>
                            </div>
                          </div>
                          <div class="preview-item-content">
                            <h6 class="preview-subject font-weight-normal">New user registration</h6>
                            <p class="font-weight-light small-text mb-0 text-muted">
                              2 days ago
                            </p>
                          </div>
                        </a>
                      </div>
                    </li>
                  </ul>
                  <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                    <span class="mdi mdi-menu"></span>
                  </button>
                </div>
                <div class="navbar-menu-wrapper">
                  
                      
                    </li>
                    
                  </ul>
                </div>
              </nav>
              <!-- partial -->
              <div class="main-panel">
                <div class="content-wrapper">
                  <div class="row">
                    <div class="container">
                      <div class="card">
                        <div class="card-header">
                          <h4 class="text-center">Data Order sandikala Batik</h4>
                        </div>
                      </div>
                    </div>
        
                    <div class="container mt-5">
                      <div class="card">
                        <div class="card-body">
                          <h3>Anda Tidak Mempunyai Hak Akses Terhadap Laman Ini</h3>
                        </div>
                      </div>
                    </div>
                    </div>
                  </div>
                  
                  <!-- row end -->
                </div>
                <!-- content-wrapper ends -->
                <!-- partial:./partials/_footer.html -->
                <footer class="footer">
                  <div class="card">
                    <div class="card-body">
                        <span class="text-dark d-block text-center text-sm-center d-sm-inline-block">Copyright © SandikalaBatik.com 2023</span>
                    </div>
                  </div>
                </footer>
                <!-- partial -->
              </div>
              <!-- main-panel ends -->
            </div>
            <!-- page-body-wrapper ends -->
          </div>
          <!-- container-scroller -->
        
          <!-- base:js -->
          <script src="{{ asset('admin') }}/vendors/js/vendor.bundle.base.js"></script>
          <!-- endinject -->
          <!-- Plugin js for this page-->
          <script src="{{ asset('admin') }}/vendors/chart.js/Chart.min.js"></script>
          <!-- End plugin js for this page-->
          <!-- inject:js -->
          <script src="{{ asset('admin') }}/js/off-canvas.js"></script>
          <script src="{{ asset('admin') }}/js/hoverable-collapse.js"></script>
          <script src="{{ asset('admin') }}/js/template.js"></script>
          <!-- endinject -->
          <!-- plugin js for this page -->
          <!-- End plugin js for this page -->
          <!-- Custom js for this page-->
          <script src="{{ asset('admin') }}/js/dashboard.js"></script>
          <!-- End custom js for this page-->
        </body>
        
        </html>        
        @endif
@endif