@extends('layouts.app')

@section('content')
<!-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->

<!doctype html>
<html lang="en">
  <head>
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <style>
      body {
        background:
        /* top, transparent black, faked with gradient */ 
        linear-gradient(
          rgba(0, 0, 0, 0.2), 
          rgba(0, 0, 0, 0.7)

        ),
        
        /* bottom, image */
        url('gambar/background_login.png');
        backdrop-filter: blur(5px);
}

button {
    justify-content: center;
    margin-left: 240px;
}
    </style>
  </head>
  <link rel="icon" href="asset/background_register.jpg">

  <body style="background-image:url('gambar/batik-kawung.png')">

  <section class="vh-100"   >
      <div class="container py-2 h-100">
          <div class="row d-flex justify-content-center align-items-center h-100">
              <div class="col col-xl-10">
                  <div class="card" style="border-radius: 1rem; background-color: #9DC183;">
                    <span class="h1 fw-bold mb-0 text-center">Sandikala Batik</span>
                  <!-- judul tengah  -->
                  <div class="row g-0">
                      <div class="col-md-6 col-lg-5 d-none d-md-block">
              <img src="gambar/background_login.png"
                alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
            </div>
            <div class="col-md-6 col-lg-7 d-flex align-items-center" style="background-color: #9DC183;">
                <div class="card-body p-4 p-lg-5 text-black ">
                    
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="d-flex align-items-center mb-3 pb-1 text-center">
                            
                            </div>

                  <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Sign into your account</h5>

                  <div class="form-outline mb-3">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror form-control-lg" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                    <label class="form-label" for="form2Example17">Email Address</label>
                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                  </div>

                    <div class="form-outline mb-2">
                      <input id="password" type="password" class="form-control @error('password') is-invalid @enderror form-control-lg" name="password" required autocomplete="current-password">
                      <label class="form-label" for="form2Example27">Password</label>
                      @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                    </div>

                  <div class="pt-1 mb-2 justify-content-center">
                    <button class="btn btn-dark btn-lg center-block justify-content-center" type="submit" name="login">Login</button>
                  </div>
                  
                  <p class="mb-5 pb-lg-2 mt-5" style="color: black;">Don't have an account? 
                  <a href="/register"
                      style="color: #393f81;" class="text-decoration-none">Register here</a></p>
                      
                      <div class="text-center">
                        <a href="https://www.instagram.com/mauas_jkm/" class="text-decoration-none">
                          <i class='fa fa-instagram m-3 p-3' style='font-size:46px; color:black'></i>
                        <a href="https://www.facebook.com/search/top?q=mauas%20jkm" class="text-decoration-none">
                        <i class='fa fa-facebook m-3 p-3' style='font-size:46px; color:black'></i>
                        </a>
                        <a href="https://www.youtube.com/@mauasjkm2880" class="text-decoration-none">
                        <i class='fa fa-youtube m-3 p-3' style='font-size:46px; color:black'></i>
                        </a>
                        </a>
                      </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
  </body>
</html>
@endsection
