@if(Auth::user())
        @if(Auth::user()->level == 1)
        <!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Admin Sandikala</title>
  <!-- base:css -->
  <link rel="stylesheet" href="admin/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="admin/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="admin/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="admin/images/favicon.png" />
</head>
<body>
  <div class="container-scroller d-flex">
    <!-- partial:./partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item sidebar-category">
          <p>Navigation</p>
          <span></span>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/">
            <i class="mdi mdi-view-quilt menu-icon"></i>
            <span class="menu-title">Dashboard</span>
            <div class="badge badge-info badge-pill">2</div>
          </a>
        </li>
        <li class="nav-item sidebar-category">
          <p>Components</p>
          <span></span>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="/TambahProduk">
            <i class="mdi mdi-view-headline menu-icon"></i>
            <span class="menu-title">Tambah Produk</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/kontak">
            <i class="mdi mdi-chart-pie menu-icon"></i>
            <span class="menu-title">Pertanyaan Customer</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about">
            <i class="mdi mdi-grid-large menu-icon"></i>
            <span class="menu-title">Produk</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/shop">
            <i class="mdi mdi-emoticon menu-icon"></i>
            <span class="menu-title">Data Customer</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/logout">
            <span class="menu-title btn btn-danger">Logout</span>
          </a>
        </li>
        
        
      </ul>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:./partials/_navbar.html -->
      <nav class="navbar col-lg-12 col-12 px-0 py-0 py-lg-4 d-flex flex-row">
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <div class="navbar-brand-wrapper">

            <a class="navbar-brand brand-logo-mini" href="index.html"><img src="images/logo-mini.svg" alt="logo"/></a>
          </div>
          <h4 class="font-weight-bold mb-0 d-none d-md-block mt-1 m-5"> Welcome back,  {{ Auth::user()->name }}</h4>
          <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item">
              <h4 class="mb-0 font-weight-bold d-none d-xl-block">Dashboard Sandikala Batik</h4>
            </li>
            <li class="nav-item dropdown mr-1">
              <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" data-toggle="dropdown">
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                <a class="dropdown-item preview-item">
                  
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal">David Grey
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      The meeting is cancelled
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                      <img src="images/faces/face2.jpg" alt="image" class="profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal">Tim Cook
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      New product launch
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                      <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal"> Johnson
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      Upcoming board meeting
                    </p>
                  </div>
                </a>
              </div>
            </li>
            <li class="nav-item dropdown mr-2">
              <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center" id="notificationDropdown" href="#" data-toggle="dropdown">
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-success">
                      <i class="mdi mdi-information mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">Application Error</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      Just now
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-warning">
                      <i class="mdi mdi-settings mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">Settings</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      Private message
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-info">
                      <i class="mdi mdi-account-box mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">New user registration</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      2 days ago
                    </p>
                  </div>
                </a>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
        <div class="navbar-menu-wrapper">
          
              
            </li>
            
          </ul>
        </div>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <h4>Data Order sandikala Batik</h4>

            <div class="container mt-5">
              <div class="card">
                <div class="card-body">
                  <table class="table bg-dark text-light">
                    <thead>
                      <tr class="bg-secondary">
                        <th scope="col"><b>Email Customer</b></th>
                        <th scope="col"><b>Action</b></th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($belanja as $o)
                      <?php $no = 0;
                      $no++;
                      $User = \App\Models\User::where('id', $o->user_id)->first();
                      $produk = \App\Models\Produk::where('id', $o->produk_id)->first();
                      ?>
                      <tr>
                        <td>{{ $User->email }}</td>
                        <td>
                        
                            @if($o->status == 0)
                            <button class="btn btn-warning">Belum Tambah Ongkir</button>
                            @endif
                            @if($o->status == 1)
                          <a href="/UpdateData/{{ $o->id }}" class="btn btn-primary">Detail Produk</a>
                          @endif
                          @if($o->status == 2)
                          <a class="btn btn-success">Produk Terverifikasi</a>
                          @endif
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            </div>
          </div>
          
          <!-- row end -->
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:./partials/_footer.html -->
        <footer class="footer">
          <div class="card">
            <div class="card-body">
                <span class="text-dark d-block text-center text-sm-center d-sm-inline-block">Copyright © SandikalaBatik.com 2023</span>
            </div>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- base:js -->
  <script src="admin/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="admin/vendors/chart.js/Chart.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="admin/js/off-canvas.js"></script>
  <script src="admin/js/hoverable-collapse.js"></script>
  <script src="admin/js/template.js"></script>
  <!-- endinject -->
  <!-- plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- Custom js for this page-->
  <script src="admin/js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>
        @endif
@endif

@if(Auth::user())
        @if(Auth::user()->level == 0)

        <!DOCTYPE html>
        <html lang="en">
          <head>
          <title>Sandikala Batik</title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
            <link rel="stylesheet" href="fonts/icomoon/style.css">
        
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/magnific-popup.css">
            <link rel="stylesheet" href="css/jquery-ui.css">
            <link rel="stylesheet" href="css/owl.carousel.min.css">
            <link rel="stylesheet" href="css/owl.theme.default.min.css">
        
        
            <link rel="stylesheet" href="css/aos.css">
        
            <link rel="stylesheet" href="css/style.css">
            
          </head>
          <body>
          
          <div class="site-wrap">
            <header class="site-navbar" role="banner">
              <div class="site-navbar-top">
                <div class="container">
                  <div class="row align-items-center">
        
                    <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
                    </div>
        
                    <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
                      <div class="site-logo">
                        <a href="/" class="js-logo-clone">Sandikala Batik</a>
                      </div>
                    </div>
        
                    <div class="col-6 col-md-4 order-3 order-md-3 text-right">
                      <div class="site-top-icons">
                        <ul>
                          <li>
                            <a href="/BelanjaUser" class="site-cart">
                              <span class="icon icon-shopping_cart"></span>
                            </a>
                          </li> 
                          <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a></li>
                        </ul>
                      </div> 
                    </div>
        
                  </div>
                </div>
              </div> 
              <nav class="site-navigation text-right text-md-center" role="navigation">
                <div class="container">
                  <ul class="site-menu js-clone-nav d-none d-md-block">
                    <li>
                      <a href="/">Home</a>
                    </li>
                    <li class="active">
                      <a href="/about">About</a>
                    </li>
                    <li><a href="/shop">Shop</a></li>
                    <li><a href="/contact">Contact</a></li>
                    <li class="text-end"> {{ Auth::user()->name }}</li>

                  </ul>
                </div>
              </nav>
            </header>
        
            <div class="bg-light py-3">
              <div class="container">
                <div class="row">
                  <div class="col-md-12 mb-0"><a href="index.html">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">About</strong></div>
                </div>
              </div>
            </div>  
        
            <div class="site-section border-bottom" data-aos="fade-up" data-aos-delay="500">
              <div class="container">
                <div class="row mb-5">
                  <div class="col-md-6">
                    <div class="block-16">
                      <figure>
                        <img src="gambar/kain.png" alt="Image placeholder" class="img-fluid ">
                        
        
                      </figure>
                    </div>
                  </div>
                  <div class="col-md-1"></div>
                  <div class="col-md-5">
                    
                    
                    <div class="site-section-heading pt-3 mb-4">
                      <h2 class="text-black">E-Commerce Sandikala Batik</h2>
                    </div>
                    <p>Sandikala Batik adalah Toko Fashion Batik KHAS Nusantara yang dimana memiliki bahan yang terbuat dari sutra sehingga sangat nyaman ketika digunakan.</p>
                    <p>Sandikala Batik memiliki produk yang di design oleh para designer yang terkenal sehingga model-model batik yang di produk tentunya mengikuti model yang sedang trend saat ini.</p>
                    
                  </div>
                </div>
              </div>
            </div>
        
            <div class="site-section border-bottom" data-aos="fade">
              <div class="container">
                <div class="row justify-content-center mb-5">
                  <div class="col-md-7 site-section-heading text-center pt-4">
                    <h2>Bahan Baku Utama Pembuatan Produk Batik Tulis</h2>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="500">
          
                    <div class="block-38 text-center">
                      <div class="block-38-img">
                        <div class="block-38-header">
                          <img src="gambar/malam.jpeg" alt="Image placeholder" class="mb-4">
                          <h3 class="block-38-heading h4">Malam/Lilin</h3>
                          <p class="block-38-subheading">Bahan Pembuatan Batik Tulis</p>
                        </div>
                        <div class="block-38-body">
                          <p>Adalah lilin yang digunakan dalam proses pembuatan batik. Lilin malam ini terbuat dari bahan gondorekum, lemak minyak kelapa, serta lilin. </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="750">
                    <div class="block-38 text-center">
                      <div class="block-38-img">
                        <div class="block-38-header">
                          <img src="gambar/Perbedaan-tekstil-dan-kain.jpeg" alt="Image placeholder" class="mb-4">
                          <h3 class="block-38-heading h4">Kain Katun</h3>
                          <p class="block-38-subheading">Bahan Pembuatan Batik Tulis</p>
                        </div>
                        <div class="block-38-body">
                          <p>Pembuatan batik dilakukan dengan memakai media kain. Jenisnya beragam, bisa memakai kain katun atau jenis lainnya yang sesuai. </p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-4" data-aos="fade-up" data-aos-delay="800">
                    <div class="block-38 text-center">
                      <div class="block-38-img">
                        <div class="block-38-header">
                          <img src="gambar/warna.png" alt="Image placeholder" class="mb-4">
                          <h3 class="block-38-heading h4">Pewarna Batik</h3>
                          <p class="block-38-subheading">Bahan Pembuatan Batik Tulis</p>
                        </div>
                        <div class="block-38-body">
                          <p>Setelah digambar, motif batik diberi warna yang cocok. Untuk pewarnaannya menggunakan bahan pewarna.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
            </div>
          
        
            <div class="site-section site-section-sm site-blocks-1">
              <div class="container">
                <div class="row">
                  <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="100">
                    <div class="icon mr-4 align-self-start">
                      <span class="icon-truck"></span>
                    </div>
                    <div class="text">
                      <h2 class="text-uppercase">Diantar Keseluruh Indonesia</h2>
                      <p>Produk dapat diantar keseluruh wilayah yang ada di Indonesia.</p>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="300">
                    <div class="icon mr-4 align-self-start">
                      <span class="icon-refresh2"></span>
                    </div>
                    <div class="text">
                      <h2 class="text-uppercase">Produk terupdate</h2>
                      <p>Produk yang dijual selalu terupdate dengan Design Batik KHAS Indonesia terbaru.</p>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="500">
                    <div class="icon mr-4 align-self-start">
                      <span class="icon-help"></span>
                    </div>
                    <div class="text">
                      <h2 class="text-uppercase">Customer Support</h2>
                      <p>Customer dapat menanyakan dan memberikan feedback kepada E-Commerce Sandikala.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        
            
            <footer class="site-footer border-top" data-aos="fade-up" data-aos-delay="500">
              <div class="container">
                <div class="row">
                  <div class="col-lg-6 mb-5 mb-lg-0">
                    <div class="row">
                      <div class="col-md-12">
                        <h3 class="footer-heading mb-4">Navigations</h3>
                      </div>
                      <div class="col-md-6 col-lg-4">
                        <ul class="list-unstyled">
                          <li><a href="">Home</a></li>
                          <li><a href="/about">About</a></li>
                          <li><a href="/shop">Shop</a></li>
                          <li><a href="/contact">Contact</a></li>
                        </ul>
                      </div>
                      <div class="col-md-6 col-lg-4">
                        
                      </div>
                      <div class="col-md-6 col-lg-4">
                        
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
                    <h3 class="footer-heading mb-4">Sandikala Batik</h3>
                    <a href="#" class="block-6">
                      <img src="gambar/batik.png" alt="Image placeholder" class="img-fluid rounded mb-4">
                      <h3 class="font-weight-light  mb-0">Finding Your Fashion Batik KHAS Nusantara</h3>
                      <p>Since 12/12/2012</p>
                    </a>
                  </div>
                  <div class="col-md-6 col-lg-3">
                    <div class="block-5 mb-5">
                      <h3 class="footer-heading mb-4">Contact Info</h3>
                      <ul class="list-unstyled">
                        <a href="https://www.google.com/maps/place/Perabot+Agung/@3.563886,80.7626519,5z/data=!4m10!1m2!2m1!1sperabot+agung!3m6!1s0x1011454328738c69:0x4b0b4386e7ff3fa0!8m2!3d3.563886!4d98.6923394!15sCg1wZXJhYm90IGFndW5nkgERY29udmVuaWVuY2Vfc3RvcmXgAQA!16s%2Fg%2F11b7hjk9t2">
                          <li class="address">HM7R+HW4, Jl. Sisingamangaraja, Teladan Bar., Kec. Medan Kota, Kota Medan, Sumatera Utara 20213</li>
                        </a>
                        <li class="phone"><a href="https://api.whatsapp.com/send?phone=6282161916411&text=Halo%20sandikala%20Batik%20Saya%20Ingin%20Bertanya">+62 821-6191-6411</a></li>
                        <li class="email">sandikalabatik@gmail.com</li>
                      </ul>
                    </div>
        
                    <div class="block-7">
                      <form action="#" method="post">
                      </form>
                    </div>
                  </div>
                </div>
                <div class="row pt-5 mt-5 text-center">
                  <div class="col-md-12">
                    <p>
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | Sandikala Batik 
                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                    </p>
                  </div>
                  
                </div>
              </div>
            </footer>
          </div>
        
          <script src="js/jquery-3.3.1.min.js"></script>
          <script src="js/jquery-ui.js"></script>
          <script src="js/popper.min.js"></script>
          <script src="js/bootstrap.min.js"></script>
          <script src="js/owl.carousel.min.js"></script>
          <script src="js/jquery.magnific-popup.min.js"></script>
          <script src="js/aos.js"></script>
        
          <script src="js/main.js"></script>
            
          </body>
        </html>
        @endif
@endif