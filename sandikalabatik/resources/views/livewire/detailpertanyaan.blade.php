<div class="container">
    <div class="card">
        <div class="card-body">
            <form>
                <div class="mb-3">
                  <label for="exampleInputEmail1" class="form-label">First Name</label>
                  <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{ $data->firstname }}">
                </div> 
                <div class="mb-3">
                  <label for="exampleInputPassword1" class="form-label">Last Name</label>
                  <input type="text" class="form-control" id="exampleInputPassword1">
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail1" class="form-label">Email</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
                  </div>
                  <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Subject</label>
                    <input type="text" class="form-control" id="exampleInputPassword1">
                  </div>
                  <div class="mb-3">
                    <label for="exampleInputPassword1" class="form-label">Message</label>
                    <input type="text" class="form-control" id="exampleInputPassword1">
                  </div>
                <a href="/kontak" type="submit" class="btn btn-primary">Kembali</a>
              </form>
        </div>
    </div>
</div>