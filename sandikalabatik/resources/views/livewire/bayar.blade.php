<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            @if($belanja->status == 1)
            <div class="row">

            </div>
            @elseif($belanja->status == 2)
                <div class="card">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col">
                                <table class="table" style="border-top : hidden">
                                    <tr>
                                        <td>Virtual Akun</td>
                                        <td>:</td>
                                        <td>{{ $va_number }}</td>
                                    </tr>
                                    <tr>
                                        <td>Bank</td>
                                        <td>:</td>
                                        <td>{{ $bank }}</td>
                                    </tr>
                                    <tr>
                                        <td>Total Harga</td>
                                        <td>:</td>
                                        <td>{{ $gross_amount }}</td>
                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td>:</td>
                                        <td>{{ $transaction_status }}</td>
                                    </tr>
                                    <tr>
                                        <td>Batas Waktu Pembayaran</td>
                                        <td>:</td>
                                        <td>{{ $deadline }}</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>
<div>

    <!-- mulai darisini -->
    <head>
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <style>
            body {
        background-image: url("gambar/background_login.png");
}
        </style>
    </head>
    <body class="bg-warning">
        


            <div class="container">
                <div class="card">
                    <div class="card-header text-center">
                        <h4><b>Pembayaran Produk</b></h4>
                    </div>
                    <div class="card-body">
                        <b>*Perhatian</b>
                        <br>
                        <p>
                            1. Pada Pembayaran akan dilakukan dengan Payment Gateway yang dimana metode pembayaran dapat dipilih setelah mengklik Tombol <b>Bayar Sekarang</b>
                            <br>
                            2. Pada Status dan Action akan masih belum berubah sampai admin Sandikala Batik melakukan konfirmasi terhadap Pembayaran yang dilakukan
                            <br>
                            3. Pada Pengembalian barang dapat dilakukan setelah Produk sampai ke alamat Tujuan
                        </p>
        
                        <div class="col-md-12">
                            <button id="pay-button" type="button" class="btn btn-primary center-block">Bayar Sekarang!</button>
                            <a href="/BelanjaUser" class="btn btn-danger">Kembali</a>
                        </div>
                                    
        
                        <form action="Payment" id="payment-form" method="get">
                            <input type="hidden" name="result_data" value=""></div>
                        </form>                       
                   </div>
                </div>
            
        </div>
        
        <script>
            AOS.init();
          </script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    
</body>

<!-- @TODO: replace SET_YOUR_CLIENT_KEY_HERE with your client key -->
<script type="text/javascript"
src="https://app.sandbox.midtrans.com/snap/snap.js"
data-client-key="SB-Mid-client-stUBZTJUPjad7oXj"></script>
<!-- Note: replace with src="https://app.midtrans.com/snap/snap.js" for Production environment -->
</head>

<body>

    <script type="text/javascript"
    src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="SB-Mid-client-stUBZTJUPjad7oXj"></script>
    <script type="text/javascript">
      // For example trigger on button clicked, or any time you need
      var payButton = document.getElementById('pay-button');
      payButton.addEventListener('click', function () {
        // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
        window.snap.pay('{{ $snapToken }}', {
          onSuccess: function(result){
            /* You may add your own implementation here */
            //alert("payment success!"); console.log(result);
            window.location.href = '/BelanjaUser';
            console.log(result);
          },
          onPending: function(result){
            /* You may add your own implementation here */
            alert("wating your payment!"); console.log(result);
          },
          onError: function(result){
            /* You may add your own implementation here */
            alert("payment failed!"); console.log(result);
          },
          onClose: function(){
            /* You may add your own implementation here */
            alert('you closed the popup without finishing the payment');
          }
        })
      });
    </script>
