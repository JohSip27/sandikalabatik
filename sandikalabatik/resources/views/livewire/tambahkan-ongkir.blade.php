<head>
    
</head>
<body class="bg-warning">

    <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            {{ ('Tambah Ongkos Kirim') }}
                        </div>
                        <div class="card-body">
                            <form wire:submit.prevent="getOngkir">
                                <label for="provinsi" class="col-md-12 col-form-label text-md-left">{{ ('Silahkan Pilih Provinsi Anda') }}</label>
                                <select name="provinsi" wire:model="provinsi" class="form-control">
                                    <option value="0">--PILIH PROVINSI--</option>
                                    @forelse($daftarProvinsi as $p)
                                    <option value="{{ $p['province_id'] }}">{{ $p['province'] }}</option>
                                    @empty
                                    <option value="0">Provinsi Tidak Ditemukan</option>
                                    @endforelse
                                </select>
    
                                <label for="kota" class="col-md-12 col-form-label text-md-left">{{ ('Silahkan Pilih Kota Anda') }}</label>
                                <select name="kota" wire:model="kota_id" class="form-control">
                                    <option value="">--PILIH KABUPATEN/KOTA--</option>
                                    @if($provinsi)
                                    @forelse($daftarKota as $k)
                                    <option value="{{ $k['city_id'] }}">{{ $k['city_name'] }}</option>
                                    @empty
                                    <option value="">PILIH KABUPATEN KOTA</option>
                                    @endforelse
                                    @endif
                                </select>
    
                                <label for="jasa" class="col-md-12 col-form-label text-md-left">{{ ('Jasa Pengiriman') }}</label>
                                <select name="jasa" wire:model="jasa" class="form-control">
                                    <option value="0">--PILIH JASA PENGIRIMAN--</option>
                                    <option value="jne">JNE</option>
                                    <option value="pos">POS Indonesia</option>
                                    <option value="tiki">TIKI</option>
                                    
                                </select>
    
                                <br><br>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-success btn-block">Lihat Daftar Ongkir</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
    
            </div>
    
    
    
    </div>
</body>