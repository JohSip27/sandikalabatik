<div>
    @if(Auth::user())
        @if(Auth::user()->level == 1)
            <!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Admin Sandikala</title>
  <!-- base:css -->
  <link rel="stylesheet" href="admin/vendors/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="admin/vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="admin/css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="admin/images/favicon.png" />
</head>
<body>
  <div class="container-scroller d-flex">
    <!-- partial:./partials/_sidebar.html -->
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
      <ul class="nav">
        <li class="nav-item sidebar-category">
          <p>Navigation</p>
          <span></span>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/">
            <i class="mdi mdi-view-quilt menu-icon"></i>
            <span class="menu-title">Dashboard</span>
            <div class="badge badge-info badge-pill">2</div>
          </a>
        </li>
        <li class="nav-item sidebar-category">
          <p>Components</p>
          <span></span>
        </li>
        
        <li class="nav-item">
          <a class="nav-link" href="/TambahProduk">
            <i class="mdi mdi-view-headline menu-icon"></i>
            <span class="menu-title">Tambah Produk</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/kontak">
            <i class="mdi mdi-chart-pie menu-icon"></i>
            <span class="menu-title">Pertanyaan Customer</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/about">
            <i class="mdi mdi-grid-large menu-icon"></i>
            <span class="menu-title">Produk</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/shop">
            <i class="mdi mdi-emoticon menu-icon"></i>
            <span class="menu-title">Data Customer</span>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/logout">
            <span class="menu-title btn btn-danger">Logout</span>
          </a>
        </li>
        
        
      </ul>
    </nav>
    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
      <!-- partial:./partials/_navbar.html -->
      <nav class="navbar col-lg-12 col-12 px-0 py-0 py-lg-4 d-flex flex-row">
        <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <div class="navbar-brand-wrapper">

            <a class="navbar-brand brand-logo-mini" href="index.html"><img src="images/logo-mini.svg" alt="logo"/></a>
          </div>
          <h4 class="font-weight-bold mb-0 d-none d-md-block mt-1 m-5"> Welcome back,  {{ Auth::user()->name }}</h4>
          <ul class="navbar-nav navbar-nav-right">
            <li class="nav-item">
              <h4 class="mb-0 font-weight-bold d-none d-xl-block">Dashboard Sandikala Batik</h4>
            </li>
            <li class="nav-item dropdown mr-1">
              <a class="nav-link count-indicator dropdown-toggle d-flex justify-content-center align-items-center" id="messageDropdown" href="#" data-toggle="dropdown">
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                <p class="mb-0 font-weight-normal float-left dropdown-header">Messages</p>
                <a class="dropdown-item preview-item">
                  
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal">David Grey
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      The meeting is cancelled
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                      <img src="images/faces/face2.jpg" alt="image" class="profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal">Tim Cook
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      New product launch
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                      <img src="images/faces/face3.jpg" alt="image" class="profile-pic">
                  </div>
                  <div class="preview-item-content flex-grow">
                    <h6 class="preview-subject ellipsis font-weight-normal"> Johnson
                    </h6>
                    <p class="font-weight-light small-text text-muted mb-0">
                      Upcoming board meeting
                    </p>
                  </div>
                </a>
              </div>
            </li>
            <li class="nav-item dropdown mr-2">
              <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center" id="notificationDropdown" href="#" data-toggle="dropdown">
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
                <p class="mb-0 font-weight-normal float-left dropdown-header">Notifications</p>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-success">
                      <i class="mdi mdi-information mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">Application Error</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      Just now
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-warning">
                      <i class="mdi mdi-settings mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">Settings</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      Private message
                    </p>
                  </div>
                </a>
                <a class="dropdown-item preview-item">
                  <div class="preview-thumbnail">
                    <div class="preview-icon bg-info">
                      <i class="mdi mdi-account-box mx-0"></i>
                    </div>
                  </div>
                  <div class="preview-item-content">
                    <h6 class="preview-subject font-weight-normal">New user registration</h6>
                    <p class="font-weight-light small-text mb-0 text-muted">
                      2 days ago
                    </p>
                  </div>
                </a>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
        <div class="navbar-menu-wrapper">
          
              
            </li>
            
          </ul>
        </div>
      </nav>
      <!-- partial -->
      <div class="main-panel">
        <div class="content-wrapper">
          <div class="row">
            <div class="col-12 col-xl-6 grid-margin stretch-card">
              <div class="row w-100 flex-grow">
                <div class="col-md-12 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <p class="card-title mt-3"><b>Sandikala Batik</b></p>
                      <div class="row mb-3 mt-5">
                        Sandikala Batik adalah suatu E-Commerce Fashion Batik yang dimana menjual berbagai kreasi Kain Batik yang bermotif KHAS Nusantara dan terbuat dari Kain Katun sehingga sangat nyaman jika digunakan nantinya.<br><br>Sandikala berasal dari bahasa Jawa yaitu Sendakala yang berarti pergantian antara Petang dan Malam.Pada Saat Sandikala berlangsung konon katanya Makhluk Halus sering mencuri anak-anak yang masih berada diluar rumah.<br><br>Orang tua akan menyuruh anaknya pulang jika sudah Maghrib agar terhindar dari kejadian yang tidak diinginkan.
                        <div class="col-md-7">
                          <div class="d-flex justify-content-between traffic-status">
                         </div>
                        </div>
                        <div class="col-md-5">
                          <ul class="nav nav-pills nav-pills-custom justify-content-md-end" id="pills-tab-custom"
                            role="tablist">
                            
                            
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <b>Visi Sandikala Batik</b>
                        <br><br>
                        1.Mengembangkan Citra Batik KHAS Nusantara.
                        <br><br>
                        2.Menjadikan Batik terkenal di belahan Dunia.
                      </div>
                      <div class="d-flex align-items-center flex-wrap mb-3">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <p class="card-title"><b>Misi Sandikala Batik</b>
                          <br><br>
                        1.Membuat Customer Terkesan atas Pelayanan Sandikala Batik.
                        <br><br>
                        2.Memberikan yang terbaik kepada Customer. 

                        </p>
                        
                      </div>
                      <div class="d-flex align-items-center flex-wrap mb-3">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 col-xl-6 grid-margin stretch-card">
              <div class="row w-100 flex-grow">
                <div class="col-md-6 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <p class="card-title"><b>Alamat Sandikala Batik</b>
                        <br><br>
                        <a href="https://www.google.com/maps/place/Perabot+Agung/@3.563886,80.7626519,5z/data=!4m10!1m2!2m1!1sperabot+agung!3m6!1s0x1011454328738c69:0x4b0b4386e7ff3fa0!8m2!3d3.563886!4d98.6923394!15sCg1wZXJhYm90IGFndW5nkgERY29udmVuaWVuY2Vfc3RvcmXgAQA!16s%2Fg%2F11b7hjk9t2">
                          <li class="address">HM7R+HW4, Jl. Sisingamangaraja, Teladan Bar., Kec. Medan Kota, Kota Medan, Sumatera Utara 20213</li>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-md-6 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body pb-0">
                      <div class="d-flex align-items-center mb-4">
                        <p class="card-title mb-0 mr-1"><b>Toko Sandikala Batik</b>
                        <br><br>
                        Sandikala batik Tidak mempunyai Toko melainkan Sandikala Batik mempunyai Rumah Produksi
                        
                        </p>
                      </div>
                      <div class="d-flex flex-wrap pt-2">
                        <div class="mr-4 mb-lg-2 mb-xl-0">
                        </div>
                        <div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-12 stretch-card">
                  <div class="card">
                    <div class="card-body pb-0">
                      <p class="card-title">Nomor Kurir Pengiriman Produk Sandikala Batik
                        <br><br>
                        <table class="table bg-dark text-light">
                          <thead>
                            <tr class="bg-secondary">
                              <th scope="col">No</th>
                              <th scope="col">Nama Kurir</th>
                              <th scope="col">Nomor Telepon</th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <th scope="row">1</th>
                              <td>JNE</td>
                              <td>(021) 29278888</td>
                            </tr>
                            <tr>
                              <th scope="row">2</th>
                              <td>Pos Indonesia</td>
                              <td>1500161 </td>
                            </tr>
                            <tr>
                              <th scope="row">3</th>
                              <td>TIKI</td>
                              <td>1500125</td>

                            </tr>
                          </tbody>
                        </table>
                      </p>
                      <div class="d-flex justify-content-between flex-wrap">
                       <div class="d-flex align-items-center flex-wrap server-status-legend mt-3 mb-3 mb-md-0">
                          
                         </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          
          <!-- row end -->
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:./partials/_footer.html -->
        <footer class="footer">
          <div class="card">
            <div class="card-body">
                <span class="text-dark d-block text-center text-sm-center d-sm-inline-block">Copyright © SandikalaBatik.com 2023</span>
            </div>
          </div>
        </footer>
        <!-- partial -->
      </div>
      <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->

  <!-- base:js -->
  <script src="admin/vendors/js/vendor.bundle.base.js"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <script src="admin/vendors/chart.js/Chart.min.js"></script>
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="admin/js/off-canvas.js"></script>
  <script src="admin/js/hoverable-collapse.js"></script>
  <script src="admin/js/template.js"></script>
  <!-- endinject -->
  <!-- plugin js for this page -->
  <!-- End plugin js for this page -->
  <!-- Custom js for this page-->
  <script src="admin/js/dashboard.js"></script>
  <!-- End custom js for this page-->
</body>

</html>
        @endif
    @endif
</div>


@if(Auth::user())
        @if(Auth::user()->level == 0)
        <!DOCTYPE html>
<html lang="en">
  <head>
    <title>Sandikala Batik</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">
    <header class="site-navbar" role="banner">
      <div class="site-navbar-top">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
            </div>

            <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
              <div class="site-logo">
                <a href="/" class="js-logo-clone">Sandikala Batik</a>
              </div>
            </div>

            <div class="col-6 col-md-4 order-3 order-md-3 text-right">
              <div class="site-top-icons">
                <ul>
                  <li>
                    <a href="/BelanjaUser" class="site-cart">
                      <span class="icon icon-shopping_cart"></span>
                      
                    </a>
                    
                  </li> 
                  
                  <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a></li>
                </ul>
              </div> 
            </div>

          </div>
        </div>
      </div> 
      <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
          <ul class="site-menu js-clone-nav d-none d-md-block">
            <li class="active">
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/about">About</a>
            </li>
            <li><a href="/shop">Shop</a></li>
            <li><a href="/contact">Contact</a></li>
            <li class="text-end"> {{ Auth::user()->name }}</li>
          </ul>
        </div>
      </nav>
    </header>

    <div class="site-blocks-cover" style="background-image: url(gambar/kain_batik.jpg);" data-aos="fade">
      <div class="container text-light " data-aos="fade-up" data-aos-delay="500">
        <div class="row align-items-start align-items-md-center justify-content-end">
          <div class="card">
            <div class="card-body">

              <div class="text-center  ">
                <h1 class="mb-2 ">Temukan Batik Anda Disini</h1>
                <div class=" text-center ">
                  <p class="mb-4 ">E-commerce Sandikala menjual beberapa Produk Fashion Batik KHAS Indonesia. </p>
                  <p>
                    <a href="/shop" class="btn btn-sm btn-primary text-light" >Shop Now</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section site-section-sm site-blocks-1">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="">
            <div class="icon mr-4 align-self-start">
              <span class="icon-truck"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">Diantar Keseluruh Indonesia</h2>
              <p>Produk dapat diantar keseluruh wilayah yang ada di Indonesia.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="200">
            <div class="icon mr-4 align-self-start">
              <span class="icon-refresh2"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">Produk terupdate</h2>
              <p>Produk yang dijual selalu terupdate dengan Design Batik KHAS Indonesia terbaru.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="400">
            <div class="icon mr-4 align-self-start">
              <span class="icon-help"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">Customer Support</h2>
              <p>Customer dapat menanyakan dan memberikan feedback kepada E-Commerce Sandikala.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center mt-5">
      <div class="col-md-7 site-section-heading text-center pt-4">
        <h2>Coming Soon Product</h2>
      </div>
    </div>    <div class="site-section site-blocks-2">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0" data-aos="fade" data-aos-delay="">
            <a class="block-2-item" href="#">
              <figure class="image">
                <img src="gambar/batik_laki.png" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Collections</span>
                <h3>Men</h3>
              </div>
            </a>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
            <a class="block-2-item" href="#">
              <figure class="image">
                <img src="gambar/batik_anak.png" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Collections</span>
                <h3>Children</h3>
              </div>
            </a>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
            <a class="block-2-item" href="#">
              <figure class="image">
                <img src="gambar/batik_wanita.png" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Collections</span>
                <h3>Women</h3>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section block-3 site-blocks-2 bg-light" data-aos="fade-up" data-aos-delay="300">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>Batik KHAS Nusantara</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="nonloop-block-3 owl-carousel">
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/6.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Mega Mendung</a></h3>
                    <p class="mb-0">Motif batik yang satu ini hadir dengan pola-pola yang berbentuk awan. Pola-pola tersebut terdiri dari kata Mega yang artinya awan dan mendung yang artinya sifat yang sabar.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/3.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Sidoluhur</a></h3>
                    <p class="mb-0">Motif batik sidoluhur dikenakan biasanya digunakan oleh pengantin wanita pada saat ketika malam pengantin atau resepsi berlangsung.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/4.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Kawung</a></h3>
                    <p class="mb-0">Batik kawung merupakan jenis batik tua yang berasal dari tanah Jawa. Motif ini adalah sebuah gambaran dari buah kawung atau yang kita kenal sebagai buah aren.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/5.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Bali</a></h3>
                    <p class="mb-0">Motif-motif yang ada di dalam batik Bali kebanyakan terinspirasi dari berbagai jenis hewan. Mulai dari kura-kura, rusa, dan juga burung bangau.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/2.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Parang Rusak</a></h3>
                    <p class="mb-0">Motif batik parang rusak memiliki arti yaitu pertarungan antara manusia yang melawan sebuah kejahatan.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section block-8"data-aos="fade-up" data-aos-delay="500">
      <div class="container">
        <div class="row justify-content-center  mb-5">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>Sandikala Batik</h2>
          </div>
        </div>
        <div class="row align-items-center">
          <div class="col-md-12 col-lg-7 mb-5">
            <a href="#"><img src="gambar/sandikala.png" alt="Image placeholder" class="img-fluid rounded"></a>
          </div>
          <div class="col-md-12 col-lg-5 text-center pl-md-5">
            <h2><a href="#">E-Commerce Sandikala</a></h2>
            <p>E-Commerce Sandikala menjual Fashion Batik KHAS Nusantara yang dimana model-model terbaru dan di design oleh para designer terknal sehingga hasil yang didapatkan akan bagus.</p>
            <p><a href="/shop" class="btn btn-primary btn-sm">Shop Now</a></p>
          </div>
        </div>
      </div>
    </div>

    <footer class="site-footer border-top" data-aos="fade-up" data-aos-delay="800">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Navigations</h3>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="">Home</a></li>
                  <li><a href="/about">About</a></li>
                  <li><a href="/shop">Shop</a></li>
                  <li><a href="/contact">Contact</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-4">
                
              </div>
              <div class="col-md-6 col-lg-4">
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
            <h3 class="footer-heading mb-4">Sandikala Batik</h3>
            <a href="#" class="block-6">
              <img src="gambar/batik.png" alt="Image placeholder" class="img-fluid rounded mb-4">
              <h3 class="font-weight-light  mb-0">Finding Your Fashion Batik KHAS Nusantara</h3>
              <p>Since 12/12/2012</p>
            </a>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-5 mb-5">
              <h3 class="footer-heading mb-4">Contact Info</h3>
              <ul class="list-unstyled">
                <a href="https://www.google.com/maps/place/Perabot+Agung/@3.563886,80.7626519,5z/data=!4m10!1m2!2m1!1sperabot+agung!3m6!1s0x1011454328738c69:0x4b0b4386e7ff3fa0!8m2!3d3.563886!4d98.6923394!15sCg1wZXJhYm90IGFndW5nkgERY29udmVuaWVuY2Vfc3RvcmXgAQA!16s%2Fg%2F11b7hjk9t2">
                  <li class="address">HM7R+HW4, Jl. Sisingamangaraja, Teladan Bar., Kec. Medan Kota, Kota Medan, Sumatera Utara 20213</li>
                </a>
                <li class="phone"><a href="https://api.whatsapp.com/send?phone=6282161916411&text=Halo%20sandikala%20Batik%20Saya%20Ingin%20Bertanya">+62 821-6191-6411</a></li>
                <li class="email">sandikalabatik@gmail.com</li>
              </ul>
            </div>

            <div class="block-7">
              <form action="#" method="post">
              </form>
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | Sandikala Batik 
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
          
        </div>
      </div>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>
        @endif
    @endif


