<!DOCTYPE html>
<html lang="en">
  <head>
  <title>Sandikala Batik</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">
    <header class="site-navbar" role="banner">
      <div class="site-navbar-top">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
            </div>

            <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
              <div class="site-logo">
                <a href="/" class="js-logo-clone">Sandikala Batik</a>
              </div>
            </div>

            <div class="col-6 col-md-4 order-3 order-md-3 text-right">
              <div class="site-top-icons">
                <ul>
                  <li>
                    <a href="/BelanjaUser" class="site-cart">
                      <span class="icon icon-shopping_cart"></span>
                    </a>
                  </li> 
                  <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a></li>
                </ul>
              </div> 
            </div>

          </div>
        </div>
      </div> 
      <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
          <ul class="site-menu js-clone-nav d-none d-md-block">
            <li>
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/about">About</a>
            </li>
            <li><a href="/shop">Shop</a></li>
            <li><a href="/contact">Contact</a></li>
            <li class="text-end"> {{ Auth::user()->name }}</li>

          </ul>
        </div>
      </nav>
    </header>

    <div class="bg-light py-3">
      <div class="container">
        <div class="row">
          <div class="col-md-12 mb-0"><a href="index.html">Home</a> <span class="mx-2 mb-0">/</span> <strong class="text-black">Cart</strong></div>
        </div>
      </div>
    </div>

    <div class="site-section">
      <div class="container">
        <div class="row mb-5">
          <form class="col-md-12" method="post">
            <div class="site-blocks-table">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th class="product-thumbnail">Image</th>
                    <th class="product-name">Product</th>
                    <th class="product-price">Price</th>
                    <th class="product-quantity">Quantity</th>
                    <th class="product-total">Status</th>
                    <th class="product-remove">Total</th>
                    <th class="product-remove">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($belanja as $pesanan)
                  <tr>
                    <td class="product-thumbnail">
                    <?php 
                            $produk = \App\Models\Produk::where('id', $pesanan->produk_id)->first();

                            $belanja = \App\Models\belanja::where('id', $pesanan->id)->first();

                        ?>
                        
                      <img src="{{ asset('storage/photos/'.$produk->gambar)}}" alt="Image" class="img-fluid">
                    </td>
                    <td class="product-name">
                      <h2 class="h5 text-black">
                        {{ $produk->nama }}
                      </h2>
                    </td>
                    <td>Rp. {{ number_format($pesanan->total_harga) }}</td>
                    <td>
                      1
                      </div>
                           
                    </td>
                    <td>
                    @if($pesanan->status == 0)
                            <strong>Pesanan Belum ditambahakan Ongkos Kirim</strong>
                            @endif
                            @if($pesanan->status == 1)
                            <strong>Pesanan Sudah ditambahakan Ongkos Kirim</strong>
                            @endif
                            @if($pesanan->status == 2)
                            <strong>Pesanan Telah dipilih Pembayarannya</strong>
                            @endif

                    </td>
                    <td>Rp. {{ number_format($pesanan->total_harga) }}</td>
                    <td>
                      @if($pesanan->status == 0)
                      <a class="btn btn-danger text-light" wire:click="destroy({{ $pesanan->id }})">Delete</a>
                      @endif
                      @if($pesanan->status == 1)
                      <a class="btn btn-danger text-light" wire:click="destroy({{ $pesanan->id }})">Delete</a>
                      @endif
                        @if($pesanan->status == 0)
                            <a href="{{url('TambahOngkir/'.$pesanan->id)}}" class="btn btn-success btn-block m-1">Tambahkan Ongkir</a>
                        @endif
                        @if($pesanan-> status == 1)
                            <a href="{{url('Bayar/'.$pesanan->id)}}" class="btn btn-primary btn-block m-1">Pilih Pembayaran</a>
                        @endif
                        @if($pesanan->status == 2)
                            <a href="/BelanjaUser"class="btn btn-primary btn-block m-1" id="pay-button">Terkonfirmasi</a>
                        @endif
                    </td>
                  </tr>
    @endforeach
                  
                </tbody>
              </table>
            </div>
          </form>
        </div>

        <script type="text/javascript"
src="https://app.sandbox.midtrans.com/snap/snap.js"
data-client-key="SB-Mid-client-stUBZTJUPjad7oXj"></script>

        <script type="text/javascript"
    src="https://app.sandbox.midtrans.com/snap/snap.js"
    data-client-key="SB-Mid-client-stUBZTJUPjad7oXj"></script>
    <script type="text/javascript">
      // For example trigger on button clicked, or any time you need
      var payButton = document.getElementById('pay-button');
      payButton.addEventListener('click', function () {
        // Trigger snap popup. @TODO: Replace TRANSACTION_TOKEN_HERE with your transaction token
        window.snap.pay('', {
          onSuccess: function(result){
            /* You may add your own implementation here */
            //alert("payment success!"); console.log(result);
            window.location.href = '/BelanjaUser';
            console.log(result);
          },
          onPending: function(result){
            /* You may add your own implementation here */
            alert("wating your payment!"); console.log(result);
          },
          onError: function(result){
            /* You may add your own implementation here */
            alert("payment failed!"); console.log(result);
          },
          onClose: function(){
            /* You may add your own implementation here */
            alert('you closed the popup without finishing the payment');
          }
        })
      });
    </script>

        

        <div class="row">
          <div class="col-md-6">
            <div class="row mb-5">
              <div class="col-md-6 mb-3 mb-md-0">
                <a href="/shop" class="btn btn-primary btn-sm btn-block">Continue Shopping</a>
              </div>
              <div class="col-md-6">
              </div>
            </div>
            <div class="row">
             
            </div>
          </div>
          

    <footer class="site-footer border-top">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Navigations</h3>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="">Home</a></li>
                  <li><a href="/about">About</a></li>
                  <li><a href="/shop">Shop</a></li>
                  <li><a href="/contact">Contact</a></li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-4">
                
              </div>
              <div class="col-md-6 col-lg-4">
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
            <h3 class="footer-heading mb-4">Sandikala Batik</h3>
            <a href="#" class="block-6">
              <img src="gambar/batik.png" alt="Image placeholder" class="img-fluid rounded mb-4">
              <h3 class="font-weight-light  mb-0">Finding Your Fashion Batik KHAS Nusantara</h3>
              <p>Since 12/12/2012</p>
            </a>
          </div>
          <div class="col-md-6 col-lg-3">
            <div class="block-5 mb-5">
              <h3 class="footer-heading mb-4">Contact Info</h3>
              <ul class="list-unstyled">
                <a href="https://www.google.com/maps/place/Perabot+Agung/@3.563886,80.7626519,5z/data=!4m10!1m2!2m1!1sperabot+agung!3m6!1s0x1011454328738c69:0x4b0b4386e7ff3fa0!8m2!3d3.563886!4d98.6923394!15sCg1wZXJhYm90IGFndW5nkgERY29udmVuaWVuY2Vfc3RvcmXgAQA!16s%2Fg%2F11b7hjk9t2">
                  <li class="address">HM7R+HW4, Jl. Sisingamangaraja, Teladan Bar., Kec. Medan Kota, Kota Medan, Sumatera Utara 20213</li>
                </a>
                <li class="phone"><a href="https://api.whatsapp.com/send?phone=6282161916411&text=Halo%20sandikala%20Batik%20Saya%20Ingin%20Bertanya">+62 821-6191-6411</a></li>
                <li class="email">sandikalabatik@gmail.com</li>
              </ul>
            </div>

            <div class="block-7">
              <form action="#" method="post">
              </form>
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | Sandikala Batik 
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
          
        </div>
      </div>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
    
  </body>
</html>