<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Sandikala Batik</title>

    <!-- Fonts -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
    <link rel="icon" href="gambar/sandikala.png">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    <livewire:scripts/>
    <script src="https://cdn.jsdelivr.net/gh/livewire/turbolinks@v0.1.x/dist/livewire-turbolinks.js" data-turbolinks-eval="false" data-turbo-eval="false"></script>

</head>
<body>
    <div id="app">
        
        <ul class="navbar-nav ms-auto">
            <!-- Authentication Links -->
            @guest
                @if (Route::has('login'))
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}"><b>{{ __('Login') }}</b></a>
                    </li> -->
                @endif

                @if (Route::has('register'))
                    <!-- <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}"><b>{{ __('Register') }}</b></a>
                    </li> -->
                @endif
            @else
                <li class="nav-item dropdown">
                    </a>

                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                        </form>
                    </div>
                </li>
            @endguest
        </ul>

        @if(!Auth::user())
        <html lang="en">
  <head>
    <title>Sandikala Batik</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> 
    <link rel="stylesheet" href="fonts/icomoon/style.css">

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/owl.theme.default.min.css">


    <link rel="stylesheet" href="css/aos.css">

    <link rel="stylesheet" href="css/style.css">
    
  </head>
  <body>
  
  <div class="site-wrap">
    <header class="site-navbar" role="banner">
      <div class="site-navbar-top">
        <div class="container">
          <div class="row align-items-center">

            <div class="col-6 col-md-4 order-2 order-md-1 site-search-icon text-left">
            </div>

            <div class="col-12 mb-3 mb-md-0 col-md-4 order-1 order-md-2 text-center">
              <div class="site-logo">
                <a href="/" class="js-logo-clone">Sandikala Batik</a>
              </div>
            </div>

            <div class="col-6 col-md-4 order-3 order-md-3 text-right">
              <div class="site-top-icons">
                <ul>
                  <li>
                    <a href="/BelanjaUser" class="site-cart">
                      <span class="icon icon-shopping_cart"></span>
                      <span class="count">2</span>
                      
                    </a>
                    
                  </li> 
                  
                  <li class="d-inline-block d-md-none ml-md-0"><a href="#" class="site-menu-toggle js-menu-toggle"><span class="icon-menu"></span></a></li>
                </ul>
              </div> 
            </div>

          </div>
        </div>
      </div> 
      <nav class="site-navigation text-right text-md-center" role="navigation">
        <div class="container">
          <ul class="site-menu js-clone-nav d-none d-md-block">
            <li class="active">
              <a href="/">Home</a>
            </li>
            <li>
              <a href="/about">About</a>
            </li>
            <li><a href="/shop">Shop</a></li>
            <li><a href="/contact">Contact</a></li>
            <li>
                
            </li>
          </ul>
        </div>
      </nav>
    </header>
    

    <div class="site-blocks-cover" style="background-image: url(gambar/kain_batik.jpg);" data-aos="fade">
      <div class="container text-light " data-aos="fade-up" data-aos-delay="500">
        <div class="row align-items-start align-items-md-center justify-content-end">
          <div class="card">
            <div class="card-body">

              <div class="text-center  ">
                <h1 class="mb-2 ">Temukan Batik Anda Disini</h1>
                <div class=" text-center ">
                  <p class="mb-4 ">E-commerce Sandikala menjual beberapa Produk Fashion Batik KHAS Indonesia. </p>
                  <p>
                    <a href="/shop" class="btn btn-sm btn-primary text-light" >Shop Now</a>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section site-section-sm site-blocks-1">
      <div class="container">
        <div class="row">
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="">
            <div class="icon mr-4 align-self-start">
              <span class="icon-truck"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">Diantar Keseluruh Indonesia</h2>
              <p>Produk dapat diantar keseluruh wilayah yang ada di Indonesia.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="200">
            <div class="icon mr-4 align-self-start">
              <span class="icon-refresh2"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">Produk terupdate</h2>
              <p>Produk yang dijual selalu terupdate dengan Design Batik KHAS Indonesia terbaru.</p>
            </div>
          </div>
          <div class="col-md-6 col-lg-4 d-lg-flex mb-4 mb-lg-0 pl-4" data-aos="fade-up" data-aos-delay="400">
            <div class="icon mr-4 align-self-start">
              <span class="icon-help"></span>
            </div>
            <div class="text">
              <h2 class="text-uppercase">Customer Support</h2>
              <p>Customer dapat menanyakan dan memberikan feedback kepada E-Commerce Sandikala.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row justify-content-center mt-5">
      <div class="col-md-7 site-section-heading text-center pt-4">
        <h2>Coming Soon Product</h2>
      </div>
    </div>    <div class="site-section site-blocks-2">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0" data-aos="fade" data-aos-delay="">
            <a class="block-2-item" href="#">
              <figure class="image">
                <img src="gambar/batik_laki.png" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Collections</span>
                <h3>Men</h3>
              </div>
            </a>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="100">
            <a class="block-2-item" href="#">
              <figure class="image">
                <img src="gambar/batik_anak.png" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Collections</span>
                <h3>Children</h3>
              </div>
            </a>
          </div>
          <div class="col-sm-6 col-md-6 col-lg-4 mb-5 mb-lg-0" data-aos="fade" data-aos-delay="200">
            <a class="block-2-item" href="#">
              <figure class="image">
                <img src="gambar/batik_wanita.png" alt="" class="img-fluid">
              </figure>
              <div class="text">
                <span class="text-uppercase">Collections</span>
                <h3>Women</h3>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section block-3 site-blocks-2 bg-light" data-aos="fade-up" data-aos-delay="300">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>Batik KHAS Nusantara</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="nonloop-block-3 owl-carousel">
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/6.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Mega Mendung</a></h3>
                    <p class="mb-0">Motif batik yang satu ini hadir dengan pola-pola yang berbentuk awan. Pola-pola tersebut terdiri dari kata Mega yang artinya awan dan mendung yang artinya sifat yang sabar.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/3.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Sidoluhur</a></h3>
                    <p class="mb-0">Motif batik sidoluhur dikenakan biasanya digunakan oleh pengantin wanita pada saat ketika malam pengantin atau resepsi berlangsung.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/4.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Kawung</a></h3>
                    <p class="mb-0">Batik kawung merupakan jenis batik tua yang berasal dari tanah Jawa. Motif ini adalah sebuah gambaran dari buah kawung atau yang kita kenal sebagai buah aren.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/5.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Bali</a></h3>
                    <p class="mb-0">Motif-motif yang ada di dalam batik Bali kebanyakan terinspirasi dari berbagai jenis hewan. Mulai dari kura-kura, rusa, dan juga burung bangau.</p>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="block-4 text-center">
                  <figure class="block-4-image">
                    <img src="gambar/2.png" alt="Image placeholder" class="img-fluid">
                  </figure>
                  <div class="block-4-text p-4">
                    <h3><a href="#">Batik Parang Rusak</a></h3>
                    <p class="mb-0">Motif batik parang rusak memiliki arti yaitu pertarungan antara manusia yang melawan sebuah kejahatan.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section block-8"data-aos="fade-up" data-aos-delay="500">
      <div class="container">
        <div class="row justify-content-center  mb-5">
          <div class="col-md-7 site-section-heading text-center pt-4">
            <h2>Sandikala Batik</h2>
          </div>
        </div>
        <div class="row align-items-center">
          <div class="col-md-12 col-lg-7 mb-5">
            <a href="#"><img src="gambar/sandikala.png" alt="Image placeholder" class="img-fluid rounded"></a>
          </div>
          <div class="col-md-12 col-lg-5 text-center pl-md-5">
            <h2><a href="#">E-Commerce Sandikala</a></h2>
            <p>E-Commerce Sandikala menjual Fashion Batik KHAS Nusantara yang dimana model-model terbaru dan di design oleh para designer terknal sehingga hasil yang didapatkan akan bagus.</p>
            <p><a href="/shop" class="btn btn-primary btn-sm">Shop Now</a></p>
          </div>
        </div>
      </div>
    </div>


  


    
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content" style="background-image: url('gambar/batik-kawung.png')">

            <section class="vh-100"   >
              <div class="container py-2 h-100">
                  <div class="row d-flex justify-content-center align-items-center h-100">
                      <div class="col col-xl-10">
                          <div class="card" style="border-radius: 1rem; background-color: #9DC183;">
                            <span class="h1 fw-bold mb-0 text-center">Sandikala Batik</span>
                          <!-- judul tengah  -->
                          <div class="row g-0">
                              <div class="col-md-6 col-lg-5 d-none d-md-block">
                      <img src="gambar/background_login.png"
                        alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
                    </div>
                    <div class="col-md-6 col-lg-7 d-flex align-items-center" style="background-color: #9DC183;">
                        <div class="card-body p-4 p-lg-5 text-black ">
                            
                            <form method="POST" action="{{ route('login') }}">
                                @csrf
                                <div class="d-flex align-items-center mb-3 pb-1 text-center">
                                    
                                    </div>
        
                          <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Sign into your account</h5>
        
                          <div class="form-outline mb-3">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror form-control-lg" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>
                            <label class="form-label" for="form2Example17">Email Address</label>
                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                          </div>
        
                            <div class="form-outline mb-2">
                              <input id="password" type="password" class="form-control @error('password') is-invalid @enderror form-control-lg" name="password" required autocomplete="current-password">
                              <label class="form-label" for="form2Example27">Password</label>
                              @error('password')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                            </div>
        
                          <div class="pt-1 mb-2 justify-content-center">
                            <button class="btn btn-dark btn-lg center-block justify-content-center" type="submit" name="login">Login</button>
                          </div>
                          
                          <p class="mb-5 pb-lg-2 mt-5" style="color: black;">Don't have an account? 
                          <a href="/register"
                              style="color: #393f81;" class="text-decoration-none">Register here</a></p>
                              
                              <div class="text-center">
                                <a href="https://www.instagram.com/mauas_jkm/" class="text-decoration-none">
                                  <i class='fa fa-instagram m-3 p-3' style='font-size:46px; color:black'></i>
                                <a href="https://www.facebook.com/search/top?q=mauas%20jkm" class="text-decoration-none">
                                <i class='fa fa-facebook m-3 p-3' style='font-size:46px; color:black'></i>
                                </a>
                                <a href="https://www.youtube.com/@mauasjkm2880" class="text-decoration-none">
                                <i class='fa fa-youtube m-3 p-3' style='font-size:46px; color:black'></i>
                                </a>
                                </a>
                              </div>
                        </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
          </div>

        </div>
      </div>
    </div>









    <div class="modal fade" id="register" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content" style="background-image: url('gambar/batik-kawung.png')">

            <section class="vh-100"   >
              <div class="container py-2 h-100">
                  <div class="row d-flex justify-content-center align-items-center h-100">
                      <div class="col col-xl-10">
                          <div class="card" style="border-radius: 1rem; background-color: #9DC183;">
                            <span class="h1 fw-bold mb-0 text-center">Sandikala Batik</span>
                          <!-- judul tengah  -->
                          <div class="row g-0">
                              <div class="col-md-6 col-lg-5 d-none d-md-block">
                      <img src="gambar/background_login.png"
                        alt="login form" class="img-fluid" style="border-radius: 1rem 0 0 1rem;" />
                    </div>
                    <div class="col-md-6 col-lg-7 d-flex align-items-center" style="background-color: #9DC183;">
                        <div class="card-body p-4 p-lg-5 text-black ">
                            
                          <form method="POST" action="{{ route('register') }}">
                            @csrf
                              <div class="d-flex align-items-center mb-3 pb-1">
                                
                                <span class="h1 fw-bold mb-0">Sandikala Batik</span>
                              </div>
            
                              <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Create Into Your Account</h5>
            
                              <div class="form-outline mb-4">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror form-control-lg" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                <label class="form-label" for="form2Example17">Name</label>
                                @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                              </div>
            
                                <div class="form-outline mb-2">
                                  <input id="email" type="email" class="form-control @error('email') is-invalid @enderror form-control-lg" name="email" value="{{ old('email') }}" required autocomplete="email">
                                  <label class="form-label" for="form2Example27">Email</label>
                                  @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                </div>

                                <div class="form-outline mb-2">
                                  <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror form-control-lg" name="alamat" value="{{ old('alamat') }}" required autocomplete="alamat">
                                  <label class="form-label" for="form2Example27">Alamat</label>
                                  @error('alamat')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                </div>
                                <div class="form-outline mb-2">
                                  <input id="password" type="password" class="form-control @error('password') is-invalid @enderror form-control-lg" name="password" required autocomplete="new-password">
                                  <label class="form-label" for="form2Example27">Password</label>
                                  @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                </div>
                                <div class="form-outline mb-2">
                                  <input id="password-confirm" type="password" class="form-control form-control-lg " name="password_confirmation" required autocomplete="new-password">
                                  <label class="form-label" for="form2Example27">Confirm Password</label>
            
                                </div>
            
                              <div class="pt-1 mb-2">
                                <button class="btn btn-dark btn-lg btn-block" type="submit" name="login">register</button>
                              </div>
            
                              <p class="mb-5 pb-lg-2" style="color: black;">Do you have an account? 
                              <a href="/login"
                                  style="color: #393f81;" class="text-decoration-none">Login here</a></p>
                                  
                                  <div class="text-center">
                                    <a href="https://www.instagram.com/mauas_jkm/" class="text-decoration-none">
                                      <i class='fa fa-instagram m-3 p-3' style='font-size:46px; color:black'></i>
                                    <a href="https://www.facebook.com/search/top?q=mauas%20jkm" class="text-decoration-none">
                                    <i class='fa fa-facebook m-3 p-3' style='font-size:46px; color:black'></i>
                                    </a>
                                    <a href="https://www.youtube.com/@mauasjkm2880" class="text-decoration-none">
                                    <i class='fa fa-youtube m-3 p-3' style='font-size:46px; color:black'></i>
                                    </a>
                                    </a>
                                  </div>
                            </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
          </div>

        </div>
      </div>
    </div>









    <footer class="site-footer border-top" data-aos="fade-up" data-aos-delay="800">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 mb-5 mb-lg-0">
            <div class="row">
              <div class="col-md-12">
                <h3 class="footer-heading mb-4">Navigations</h3>
              </div>
              <div class="col-md-6 col-lg-4">
                <ul class="list-unstyled">
                  <li><a href="">Home</a></li>
                  <li><a href="/about">About</a></li>
                  <li><a href="/shop">Shop</a></li>
                  <li><a href="/contact">Contact</a></li>
                  <li>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" data-bs-whatever="@getbootstrap">Login</button>

                  </li>
                  <li>
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#register" data-bs-whatever="@getbootstrap">Register</button>

                  </li>
                </ul>
              </div>
              <div class="col-md-6 col-lg-4">
                
              </div>
              <div class="col-md-6 col-lg-4">
                
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 mb-4 mb-lg-0">
            <h3 class="footer-heading mb-4">Sandikala Batik</h3>
            <a href="#" class="block-6">
              <img src="gambar/batik.png" alt="Image placeholder" class="img-fluid rounded mb-4">
              <h3 class="font-weight-light  mb-0">Finding Your Fashion Batik KHAS Nusantara</h3>
              <p>Since 12/12/2012</p>
            </a>
          </div>
          
          <div class="col-md-6 col-lg-3">
            <div class="block-5 mb-5">
              <h3 class="footer-heading mb-4">Contact Info</h3>
              <ul class="list-unstyled">
                <a href="https://www.google.com/maps/place/Perabot+Agung/@3.563886,80.7626519,5z/data=!4m10!1m2!2m1!1sperabot+agung!3m6!1s0x1011454328738c69:0x4b0b4386e7ff3fa0!8m2!3d3.563886!4d98.6923394!15sCg1wZXJhYm90IGFndW5nkgERY29udmVuaWVuY2Vfc3RvcmXgAQA!16s%2Fg%2F11b7hjk9t2">
                  <li class="address">HM7R+HW4, Jl. Sisingamangaraja, Teladan Bar., Kec. Medan Kota, Kota Medan, Sumatera Utara 20213</li>
                </a>
                <li class="phone"><a href="https://api.whatsapp.com/send?phone=6282161916411&text=Halo%20sandikala%20Batik%20Saya%20Ingin%20Bertanya">+62 821-6191-6411</a></li>
                <li class="email">sandikalabatik@gmail.com</li>
              </ul>
            </div>

            <div class="block-7">
              <form action="#" method="post">
              </form>
            </div>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <p>
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            Copyright &copy;<script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script>document.write(new Date().getFullYear());</script> All rights reserved | Sandikala Batik 
            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
            </p>
          </div>
          
        </div>
      </div>
    </footer>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.magnific-popup.min.js"></script>
  <script src="js/aos.js"></script>

  <script src="js/main.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
    
  </body>
</html>

        @endif

        <main class="py-4">
            @yield('content')
            
        </main>
    </div>
    
</body>
</html>


